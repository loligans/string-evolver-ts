import { Candidate } from './candidate';

export enum CrossoverType {
  SinglePoint,
  MultiPoint
}

export class StringEvolver {
  public readonly MaximumFitness: number;
  private _Solution: string;
  get Solution(): string {
    return this._Solution;
  }
  private _Population: Array<Candidate>;
  get Population(): Array<Candidate> {
    return this._Population;
  }
  private _Generation: number;
  get Generation(): number {
    return this._Generation;
  }
  private _PopulationFitness: number;
  get PopulationFitness(): number {
    return this._PopulationFitness;
  }
  private _CrossoverType: CrossoverType = CrossoverType.SinglePoint;
  get CrossoverType(): CrossoverType {
    return this._CrossoverType;
  }
  set CrossoverType(value: CrossoverType) {
    if (value in CrossoverType) {
      this._CrossoverType = value;
    }
  }
  private _CrossoverPoints: number = 1;
  get CrossoverPoints(): number {
    return this._CrossoverPoints;
  }
  set CrossoverPoints(value: number) {
    if (value <= 0 && value >= this.Solution.length) return; 
    this._CrossoverPoints = value;
  }
  private _MutationRate: number = 50;
  get MutationRate(): number {
    return this._MutationRate;
  }
  set MutationRate(value: number) {
    if (value < 0 || value > 100) return;
    this._MutationRate = value
  }

  constructor(popSize: number, solution: string) {
    if (popSize % 2 === 1) popSize += 1;
    this._Solution = solution;
    this._Population = this.generatePopulation(popSize);
    this._Generation = 0;
    this.MaximumFitness = solution.length * Candidate.GeneRange;
  }
  private randomInt(min: number, max: number, includeMax: boolean = false): number {
    return Math.floor(Math.random() * (max - min + (includeMax ? 1 : 0)) + min);
  }
  private generateChromosome(length: number): Buffer {
    // Build the chromosome
    var randomChromosome: string = '';
    var randomGene: number;
    for (var i = 0; i < length; i++)
    {
      randomGene = this.randomInt(Candidate.MinGene, Candidate.MaxGene);
      randomChromosome += String.fromCharCode(randomGene);
    }

    // Returns the chromosome as a buffer
    return Buffer.from(randomChromosome, 'utf8');
  }
  private generatePopulation(popSize: number): Array<Candidate> {
    // Initialize our population and declare chromosome and fitness.
    var population = new Array<Candidate>(popSize);
    var chromosome: Buffer;

    // Fill the population with candidates
    for(var i = 0; i < popSize; i++) {
      chromosome = this.generateChromosome(this._Solution.length);
      population[i] = new Candidate([...chromosome], this._Solution);
      this._PopulationFitness += population[i].Fitness;
    }

    // Sort population by fitness
    population.sort((a, b) => b.Fitness - a.Fitness);
    return population;
  }
  private selectParents(): Array<Candidate> {
    // Initialize the parents collection
    var numParents: number = this.Population.length * .80;
    var parents = new Array<Candidate>();
    
    // Calculate the stochastic interval and starting point
    var interval: number = this.PopulationFitness / numParents;
    var startingPoint = this.randomInt(0, interval);

    // Calculate the points 
    var points = [];
    for (var i = 0; i < numParents; i++) {
      points.push(startingPoint + (i * interval));
    }

    // Stochastic universal sampling
    var fitnessSum: number = 0;
    var index: number;
    for (var p of points) {
      index = 0;
      fitnessSum = 0;
      while (fitnessSum < p) {
        fitnessSum += this.Population[index].Fitness
        index++;
      }
      parents.push(this.Population[index]);
    }
    
    return parents;
  }

  private crossover(parents: Array<Candidate>): Array<Candidate> {
    switch(this.CrossoverType) {
      case CrossoverType.SinglePoint:
        return this.singlepointCrossover(parents);
      case CrossoverType.MultiPoint:
        return this.multipointCrossover(parents);
      default:
        return this.singlepointCrossover(parents);
    }
  }

  private singlepointCrossover(parents: Array<Candidate>): Array<Candidate> {
    var children = new Array<Candidate>();
    var parent1: Candidate;
    var parent2: Candidate;
    var crossLocation: number;
    for (var i = 0; i < parents.length; i += 2) {
      // Selects the starting point for crossover
      crossLocation = this.randomInt(1, this._Solution.length);
      parent1 = this.Population[i];
      parent2 = this.Population[i+1];

      // Swap genes and add children
      children.push(
        new Candidate([
          ...parent1.Chromosome.slice(0, crossLocation), 
          ...parent2.Chromosome.slice(crossLocation)
        ], this._Solution), 
        new Candidate([
          ...parent2.Chromosome.slice(0, crossLocation), 
          ...parent1.Chromosome.slice(crossLocation)
        ], this._Solution)
      );
    }

    return children;
  }

  private multipointCrossover(parents: Array<Candidate>): Array<Candidate> {
    let children = new Array<Candidate>();
    for(let i = 0; i < parents.length; i+= 2) {
      let crossLocations: Array<number> = [];
      let parent1: Candidate = this.Population[i];
      let parent2: Candidate = this.Population[i+1];
      let child1Chromosome: Array<number> = [];
      let child2Chromosome: Array<number> = [];
      
      // Determine crossover points
      for(let z = 0; z < this.CrossoverPoints; z++) {
        let location: number = this.randomInt(0, this._Solution.length, true);
        crossLocations.push(location);
      }
      // Sort crossover points Ascending
      crossLocations.sort((a, b) => a - b);

      // Perform crossover
      for(let z = 0; z < crossLocations.length; z++) {
        let crossoverStart: number;
        let crossoverEnd: number;

        // First crossover point 
        if(z === 0) {
          if(crossLocations[z] > 0) {
            crossoverStart = 0;
            crossoverEnd = crossLocations[z];
          } else {
            crossoverStart = 0;
            crossoverEnd = 0
          }
        }
        // Final crossover point
        else if (z === crossLocations.length - 1) {
          crossoverStart = crossLocations[z - 1];
          crossoverEnd = this.Solution.length;
        }
        // Ignore Repeating crossover points
        else if(crossLocations[z - 1] === crossLocations[z]) continue;
        // All other crossover points
        else {
          crossoverStart = crossLocations[z - 1];
          crossoverEnd = crossLocations[z];
        }

        // Perform crossover
        child1Chromosome.push(...parent2.Chromosome.slice(crossoverStart, crossoverEnd));
        child2Chromosome.push(...parent1.Chromosome.slice(crossoverStart, crossoverEnd));
      }
      children.push(
        new Candidate(child1Chromosome, this.Solution), 
        new Candidate(child2Chromosome, this.Solution));
    }
    return children;
  }

  private mutate(children: Array<Candidate>): void {
    var mutate: boolean;
    var location: number;
    for(var child of children) {
      mutate = this.MutationRate >= this.randomInt(1, 100, true) ? true : false;
      if (mutate) {
        // Mutate the child in random location
        location = this.randomInt(0, this._Solution.length);
        child.Chromosome[location] = this.randomInt(Candidate.MinGene, Candidate.MaxGene);
      }
    }
  }
  private selectSurvivors(): Array<Candidate> {
    const popSize: number = this.Population.length;
    const topPercentCandidates = popSize * .20;
    // Maybe choose a better selection method later on.
    return this.Population.slice(0, topPercentCandidates);
  }
  private sortPopulationFitness() {
    this._PopulationFitness = 0;
    for (var candidate of this.Population) {
      this._PopulationFitness += candidate.Fitness;
    }
    this.Population.sort((a, b) => b.Fitness - a.Fitness);
  }
  public computeGeneration(): boolean {
    // Create the children of the next generation
    var parents = this.selectParents();
    var children = this.crossover(parents);
    this.mutate(children);

    // Determine who makes it to the next generation
    var survivors = this.selectSurvivors();

    // Update the population with the new children and survivors then resort
    this._Population = new Array<Candidate>(...children, ...survivors);
    this._Generation += 1;
    this.sortPopulationFitness();

    // Check for the solution
    var solution = this.Population[0].Fitness >= this.MaximumFitness;

    // Determine if solution was found
    return solution;
  }
}