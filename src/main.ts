import { StringEvolver } from './string-evolver';

const solution: string = 'How are you gentleman? All your base are belong to us. Warning long computation...';
const GA: StringEvolver = new StringEvolver(20, solution);
let timer: number = Date.now();
let foundSolution: boolean = false;
for (var i = 0; i < Number.MAX_SAFE_INTEGER; i++)
{
  foundSolution = GA.computeGeneration();
  if (Date.now() - timer > 50)
  {
    timer = Date.now();
    console.log(`Generation ${i}: ${String.fromCharCode(...GA.Population[0].Chromosome)}`);
  }
  if (foundSolution) {
    console.log(`Generation ${i}: ${String.fromCharCode(...GA.Population[0].Chromosome)}`);
    var genPerSec = i / process.uptime();
    console.log(`It took ${i} generations to the solution! ${genPerSec.toFixed(3)} Generations/s`);
    break;
  }
}